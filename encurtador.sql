-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 17-Dez-2020 às 00:03
-- Versão do servidor: 10.4.16-MariaDB
-- versão do PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `encurtador`
--

DELIMITER $$
--
-- Procedimentos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `alteraSenha` (IN `usuario` VARCHAR(50), IN `resposta_usuario` VARCHAR(50), IN `senha_nova` VARCHAR(50))  BEGIN
	
    DECLARE resposta_correta VARCHAR(50);
    
    START TRANSACTION;
    
        SELECT resposta INTO resposta_correta FROM usuarios WHERE nome = usuario;

        UPDATE usuarios SET senha = md5(senha_nova) WHERE nome = usuario; 

        IF resposta_correta = md5(resposta_usuario) THEN 
            SELECT 1 AS res;
            COMMIT;
        ELSE 
            SELECT 0 AS res;
            ROLLBACK;
        END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `loginSP` (IN `usuario` VARCHAR(50), IN `senha_usuario` VARCHAR(50), IN `ip_usuario` VARCHAR(50))  BEGIN
	
    DECLARE senha_correta VARCHAR(50); 
    DECLARE data_atual DATE; 
    DECLARE hora_atual TIME;
    
    START TRANSACTION;
    
        SET data_atual = CURDATE(); 
        SET hora_atual = NOW(); 
        SELECT senha INTO senha_correta FROM usuarios WHERE nome = usuario;

        UPDATE usuarios SET data_login = data_atual, hora_login = hora_atual, IP = ip_usuario WHERE nome = usuario; 

        IF senha_correta = md5(senha_usuario) THEN 
            SELECT 1 AS res;
            COMMIT;
        ELSE 
            SELECT 0 AS res;
            ROLLBACK;
        END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico`
--

CREATE TABLE `historico` (
  `id` int(11) NOT NULL,
  `nome_usuario` varchar(50) NOT NULL,
  `data_login` date NOT NULL,
  `hora_login` time NOT NULL,
  `ip` varchar(50) NOT NULL,
  `evento` varchar(50) NOT NULL,
  `nrano` int(11) NOT NULL DEFAULT 9
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4
PARTITION BY RANGE (`nrano`)
(
PARTITION p1 VALUES LESS THAN (2021) ENGINE=InnoDB,
PARTITION p2 VALUES LESS THAN (2026) ENGINE=InnoDB,
PARTITION p3 VALUES LESS THAN MAXVALUE ENGINE=InnoDB
);

-- --------------------------------------------------------

--
-- Estrutura da tabela `urls`
--

CREATE TABLE `urls` (
  `id` int(11) NOT NULL,
  `codigo` varchar(7) NOT NULL,
  `url_original` varchar(150) NOT NULL DEFAULT '',
  `url_curta` varchar(150) NOT NULL,
  `id_usuario` int(30) DEFAULT NULL,
  `acessos` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario_id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `pergunta` varchar(60) NOT NULL,
  `resposta` varchar(60) NOT NULL,
  `data_login` date DEFAULT NULL,
  `hora_login` time DEFAULT NULL,
  `IP` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Acionadores `usuarios`
--
DELIMITER $$
CREATE TRIGGER `ultimo_login` AFTER UPDATE ON `usuarios` FOR EACH ROW BEGIN
	DECLARE usuario VARCHAR(50);
    DECLARE data DATE;
    DECLARE hora TIME;
    DECLARE ip VARCHAR(50);
    SET data = NEW.data_login;
    SET hora = NEW.hora_login;
    SET ip = NEW.IP;
	IF OLD.data_login <> NEW.data_login OR OLD.hora_login <> NEW.hora_login THEN
    SELECT nome INTO usuario FROM usuarios WHERE data_login = data AND hora_login = hora;
    	INSERT INTO historico(nome_usuario, data_login, hora_login, ip, evento, nrano)
        VALUES(usuario, data, hora, ip, 'LOGIN', YEAR(data));
    END IF;

END
$$
DELIMITER ;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `historico`
--
ALTER TABLE `historico`
  ADD PRIMARY KEY (`id`,`nrano`);

--
-- Índices para tabela `urls`
--
ALTER TABLE `urls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `nome` (`nome`),
  ADD KEY `dataIDX` (`data_login`),
  ADD KEY `horaIDX` (`hora_login`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `historico`
--
ALTER TABLE `historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `urls`
--
ALTER TABLE `urls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
