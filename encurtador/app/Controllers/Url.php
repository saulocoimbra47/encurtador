<?php namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Url extends BaseController
{
	use ResponseTrait;

	public function index()
	{

		if( $this->isLoggedIn() ){
			
			$id_usuario  = $this->session->get('usuario_id');
			
			
			$urlModel = new \App\Models\UrlModel();
			$infoUrl = $urlModel->where('id_usuario' , $id_usuario)->find();


			$dados = [
				'titulo' => 'Lista de URLs',
				'infoUrl' => $infoUrl
			];
			return view('listaURLs.php', $dados);
		}
		else{
			return redirect()->to(base_url());
		}



	}

	public function Encurtar(){

		if($this->request->isAjax()){
			
			$urlOriginal = $this->request->getPost('urlOriginal');
			$codigo = substr(md5(time().$urlOriginal), 0, 7);
			$insereUrl = new \App\Models\UrlModel();
			$urlCurta = base_url('/'.$codigo);
			$consultaCodigo = new \App\Models\UrlModel();
			if($consultaCodigo->where('codigo', $codigo)->find()){
				$ncodigo = substr(md5(time().$codigo), 0, 7);
				$codigo = $ncodigo;
			}


			if( $this->isLoggedIn() ){
				
				$id_usuario = $this->session->get('usuario_id');

				$dados = [
					'codigo' => $codigo,
					'url_original' => $urlOriginal,
					'url_curta' => $urlCurta,
					'id_usuario' => $id_usuario,
					'acessos' => 0
		
				];

				if($insereUrl->insert($dados)){
					$result = [];
					$r = ['urlCurta' => $urlCurta, 'validade' => 1];
					$result[] = $r;
		
					return $this->setResponseFormat('json')->respond($result);
				}
				else{
					$result = [];
					$r = ['urlCurta' => $urlCurta, 'validade' => 0];
					$result[] = $r;
		
					return $this->setResponseFormat('json')->respond($result);
				}

					

			}
			else{

				$id_usuario = NULL;

				$dados = [
					'codigo' => $codigo,
					'url_original' => $urlOriginal,
					'url_curta' => $urlCurta,
					'id_usuario' => $id_usuario,
					'acessos' => 0
		
				];

				if($insereUrl->insert($dados)){
					$result = [];
					$r = ['urlCurta' => $urlCurta, 'validade' => 1];
					$result[] = $r;
		
					return $this->setResponseFormat('json')->respond($result);
				}
				else{
					$result = [];
					$r = ['urlCurta' => $urlCurta, 'validade' => 0];
					$result[] = $r;
		
					return $this->setResponseFormat('json')->respond($result);
				};	
			}


			
		}
		else{
			throw new PageNotFoundException();
		}
		
	}

	public function Redirecionar($codigo){
		
		$redirecionaUrl = new \App\Models\UrlModel();

		$consultaUrl = $redirecionaUrl->where('codigo', $codigo)->find();
		$acessos = $consultaUrl[0]->acessos;
		$id = $consultaUrl[0]->id;
		$acessos += 1;

		$dados = ['acessos' => $acessos];

		if($redirecionaUrl->where('codigo', $codigo)->find()){

			$url_original = $consultaUrl[0]->url_original;
			$redirecionaUrl->where('codigo', $codigo)->update($id, $dados);

			return redirect()->to($url_original);

		}
		else{
			$this->session->setFlashData('msgErroURL', 'URL original não encontrada!');
			return redirect()->to(base_url());
		}

	}


}
